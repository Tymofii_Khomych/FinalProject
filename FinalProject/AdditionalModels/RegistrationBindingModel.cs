﻿using System.ComponentModel.DataAnnotations;

namespace FinalProject.AdditionalModels
{
    public class RegistrationBindingModel
    {
        [Required(ErrorMessage = "Enter login")]
        [Display(Name = "Login")]
        [RegularExpression(@"^[A-Za-z0-9]+([A-Za-z0-9]*|[_]?[A-Za-z0-9]+)*$",
            ErrorMessage = @"Only letters of the Latin alphabet, numbers and '-'")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Enter email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter password")]
        [Display(Name = "Password")]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{12,}$",
            ErrorMessage = "Minimum 12 characters, one uppercase, one lowercase, one digit, one special character")]
        public string Password { get; set; }
    }
}
