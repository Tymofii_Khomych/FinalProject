﻿using FinalProject.Models;

namespace FinalProject.AdditionalModels
{
    public class SearchViewModel
    {
        public string SearchOption { get; set; }
        public string SearchText { get; set; }
        public List<Supplier> SupplierResults { get; set; }
        public List<Category> CategoryResults { get; set; }
        public List<Product> ProductResults { get; set; }
    }
}
