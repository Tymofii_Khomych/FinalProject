﻿namespace FinalProject.AdditionalModels
{
    public class SupplierViewModel
    {
        public string? ContactName { get; set; }
        public string CompanyName { get; set; } = null!;
        public string? City { get; set; }
        public string? Country { get; set; }
        public string ProductName { get; set; } = null!;
        public string? QuantityPerUnit { get; set; }
    }
}
