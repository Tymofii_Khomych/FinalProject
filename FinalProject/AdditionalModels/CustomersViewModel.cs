﻿using FinalProject.Models;
namespace FinalProject.AdditionalModels
{
    public class CustomersViewModel
    {
        public List<Customer>? Customers { get; set; }
        public List<string>? Countries { get; set;  }
    }
}
