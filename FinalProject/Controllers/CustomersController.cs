﻿using FinalProject.AdditionalModels;
using FinalProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace FinalProject.Controllers
{
    public class CustomersController : Controller
    {
        private readonly NorthwindContext _northwindContext;
        private string _countries = "countries";

        public CustomersController (NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public async Task<IActionResult> Index()
        {
            var model = new CustomersViewModel();
            model.Customers = await _northwindContext.Customers.Take(50).OrderBy(c => c.ContactName).ToListAsync();
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }

        public IActionResult Reset()
        {
            HttpContext.Session.Remove("SelectedCountries");
            return RedirectToAction("Index");
        }


        [HttpPost]
        public async Task<IActionResult> ChooseFromCountries(string[] selectedCountries)
        {
            if(selectedCountries.Length == 0)
            {
                return RedirectToAction("Index", "Customers");
            }
            else
            {
                var model = new CustomersViewModel();
                model.Customers = await _northwindContext.Customers
                    .Where(c => selectedCountries.Contains(c.Country))
                    .OrderBy(c => c.Country)
                    .ToListAsync();

                model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
                HttpContext.Session.SetString("SelectedCountries", string.Join(",", selectedCountries));
                return View("Index", model);
            }
        }

        public async Task<IActionResult> OrderByName()
        {
            string selectedCountriesString = HttpContext.Session.GetString("SelectedCountries");
            var model = new CustomersViewModel();

            if (selectedCountriesString != null)
            {
                string[] selectedCountries = selectedCountriesString.Split(',');

                model.Customers = await _northwindContext.Customers
                        .Where(c => selectedCountries.Contains(c.Country))
                        .OrderBy(c => c.ContactName)
                        .ToListAsync();
            }
            else
            {
                model.Customers = await _northwindContext.Customers
                        .OrderBy(c => c.ContactName)
                        .ToListAsync();
            }
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderById()
        {
            string selectedCountriesString = HttpContext.Session.GetString("SelectedCountries");
            var model = new CustomersViewModel();

            if (selectedCountriesString != null)
            {
                string[] selectedCountries = selectedCountriesString.Split(',');

                model.Customers = await _northwindContext.Customers
                        .Where(c => selectedCountries.Contains(c.Country))
                        .OrderBy(c => c.CustomerId)
                        .ToListAsync();
            }
            else
            {
                model.Customers = await _northwindContext.Customers
                        .OrderBy(c => c.CustomerId)
                        .ToListAsync();
            }
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByCompany()
        {
            string selectedCountriesString = HttpContext.Session.GetString("SelectedCountries");
            var model = new CustomersViewModel();

            if (selectedCountriesString != null)
            {
                string[] selectedCountries = selectedCountriesString.Split(',');

                model.Customers = await _northwindContext.Customers
                        .Where(c => selectedCountries.Contains(c.Country))
                        .OrderBy(c => c.CompanyName)
                        .ToListAsync();
            }
            else
            {
                model.Customers = await _northwindContext.Customers
                        .OrderBy(c => c.CompanyName)
                        .ToListAsync();
            }
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByAdress()
        {
            string selectedCountriesString = HttpContext.Session.GetString("SelectedCountries");
            var model = new CustomersViewModel();

            if (selectedCountriesString != null)
            {
                string[] selectedCountries = selectedCountriesString.Split(',');

                model.Customers = await _northwindContext.Customers
                        .Where(c => selectedCountries.Contains(c.Country))
                        .OrderBy(c => c.Address)
                        .ToListAsync();
            }
            else
            {
                model.Customers = await _northwindContext.Customers
                        .OrderBy(c => c.Address)
                        .ToListAsync();
            }
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByCity()
        {
            string selectedCountriesString = HttpContext.Session.GetString("SelectedCountries");
            var model = new CustomersViewModel();

            if (selectedCountriesString != null)
            {
                string[] selectedCountries = selectedCountriesString.Split(',');

                model.Customers = await _northwindContext.Customers
                        .Where(c => selectedCountries.Contains(c.Country))
                        .OrderBy(c => c.City)
                        .ToListAsync();
            }
            else
            {
                model.Customers = await _northwindContext.Customers
                        .OrderBy(c => c.City)
                        .ToListAsync();
            }
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByCountry()
        {
            string selectedCountriesString = HttpContext.Session.GetString("SelectedCountries");
            var model = new CustomersViewModel();

            if (selectedCountriesString != null)
            {
                string[] selectedCountries = selectedCountriesString.Split(',');

                model.Customers = await _northwindContext.Customers
                        .Where(c => selectedCountries.Contains(c.Country))
                        .OrderBy(c => c.Country)
                        .ToListAsync();
            }
            else
            {
                model.Customers = await _northwindContext.Customers
                        .OrderBy(c => c.Country)
                        .ToListAsync();
            }
            model.Countries = await _northwindContext.Customers.Select(c => c.Country).Distinct().ToListAsync();
            return View("Index", model);
        }
    }
}
