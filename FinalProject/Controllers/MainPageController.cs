﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using FinalProject.AdditionalModels;
using Microsoft.EntityFrameworkCore;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    public class MainPageController : Controller
    {
        private readonly NorthwindContext _northwindContext;

        public MainPageController(NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Autorisation");
        }

        [HttpPost]
        public async Task<IActionResult> Search(string searchOption, string searchText)
        {
            var model = new SearchViewModel
            {
                SearchOption = searchOption,
                SearchText = searchText,
                SupplierResults = new List<Supplier>(),
                CategoryResults = new List<Category>(),
                ProductResults = new List<Product>()
            };

            switch (searchOption)
            {
                case "supplier":
                    model.SupplierResults = await _northwindContext.Suppliers
                        .Where(s => s.ContactName.Contains(searchText))
                        .ToListAsync();

                    if (model.SupplierResults.Count == 0)
                    {
                        return View("NotFound", model);
                    }
                    else
                    {
                        return View("Searchpage", model);
                    }

                case "category":
                    model.CategoryResults = await _northwindContext.Categories
                        .Where(c => c.CategoryName.Contains(searchText))
                        .ToListAsync();

                    if (model.CategoryResults.Count == 0)
                    {
                        return View("NotFound", model);
                    }
                    else
                    {
                        return View("Searchpage", model);
                    }

                case "product":
                    model.ProductResults = await _northwindContext.Products
                        .Where(p => p.ProductName.Contains(searchText))
                        .ToListAsync();

                    if (model.ProductResults.Count == 0)
                    {
                        return View("NotFound", model);
                    }
                    else
                    {
                        return View("Searchpage", model);
                    }
                default:
                    return View("Notfound", model);
            }
        }
    }
}
