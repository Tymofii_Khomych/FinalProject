﻿using FinalProject.AdditionalModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FinalProject.Models;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace FinalProject.Controllers
{
    public class AdminController : Controller
    {
        private readonly NorthwindContext _northwindContext;

        public AdminController(NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Customer()
        {
            var model = await _northwindContext.Customers.Take(50).OrderBy(c => c.ContactName).ToListAsync();
            return View(model);
        }

        public async Task<IActionResult> Supplier()
        {
            var model = await _northwindContext.Suppliers.ToListAsync();
            return View(model);
        }

        public async Task<IActionResult> Product()
        {
            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductId, p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductId = grouped.Key.ProductId,
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var model = await query.ToListAsync();

            return View(model);
        }

        public async Task<IActionResult> CustomerRedact(Customer customerData)
        {
            var customerToRedact = await _northwindContext.Customers.FirstOrDefaultAsync(c => c.CustomerId == customerData.CustomerId);

            if (customerToRedact == null)
            {
                TempData["ErrorMessageRedact"] = "Customer not found";
                return RedirectToAction("Customer");
            }
            else
            {
                if (customerData.CompanyName != null)
                {
                    customerToRedact.CompanyName = customerData.CompanyName;
                }
                if (customerData.ContactName != null)
                {
                    customerToRedact.ContactName = customerData.ContactName;
                }
                if (customerData.ContactTitle != null)
                {
                    customerToRedact.ContactTitle = customerData.ContactTitle;
                }
                if (customerData.Address != null)
                {
                    customerToRedact.Address = customerData.Address;
                }
                if (customerData.City != null)
                {
                    customerToRedact.City = customerData.City;
                }
                if (customerData.Region != null)
                {
                    customerToRedact.Region = customerData.Region;
                }
                if (customerData.PostalCode != null)
                {
                    customerToRedact.PostalCode = customerData.PostalCode;
                }
                if (customerData.Country != null)
                {
                    customerToRedact.Country = customerData.Country;
                }
                if (customerData.Phone != null)
                {
                    customerToRedact.Phone = customerData.Phone;
                }
                if (customerData.Fax != null)
                {
                    customerToRedact.Fax = customerData.Fax;
                }

                TempData["SuccessMessageRedact"] = "Entity was updated";
                _northwindContext.SaveChanges();

                return RedirectToAction("Customer");
            }
        }

        public async Task<IActionResult> CustomerDelete(string CustomerId)
        {
            var customerToDelete = await _northwindContext.Customers.FirstOrDefaultAsync(c => c.CustomerId == CustomerId);

            if (customerToDelete == null)
            {
                TempData["ErrorMessageDelete"] = "Customer not found";
                return RedirectToAction("Customer");
            }
            else
            {
                var ordersToDelete = await _northwindContext.Orders.Where(o => o.CustomerId == customerToDelete.CustomerId).ToListAsync();

                foreach (var order in ordersToDelete)
                {
                    var orderDetailsToDelete = await _northwindContext.OrderDetails.Where(od => od.OrderId == order.OrderId).ToListAsync();
                    _northwindContext.OrderDetails.RemoveRange(orderDetailsToDelete);
                }

                _northwindContext.Orders.RemoveRange(ordersToDelete);
                _northwindContext.Customers.Remove(customerToDelete);


                TempData["SuccessMessageDelete"] = "User was deleted";
                _northwindContext.SaveChanges();

                return RedirectToAction("Customer");
            }
        }

        public async Task<IActionResult> CustomerAdd(Customer newCustomer)
        {
            if (ModelState.IsValid)
            {
                _northwindContext.Customers.Add(newCustomer);
                await _northwindContext.SaveChangesAsync();
                TempData["SuccessMessageAdd"] = "Customer added successfully";
            }
            else
            {
                TempData["ErrorMessageAdd"] = "Invalid customer data";
            }

            return RedirectToAction("Customer");
        }

        public async Task<IActionResult> ProductRedact(Product productData)
        {
            var productToRedact = await _northwindContext.Products.FirstOrDefaultAsync(p => p.ProductId == productData.ProductId);

            if (productToRedact == null)
            {
                TempData["ErrorMessageRedact"] = "Product not found";
                return RedirectToAction("Product");
            }
            else
            {
                if (productData.ProductName != null)
                {
                    productToRedact.ProductName = productData.ProductName;
                }
                if (productData.QuantityPerUnit != null)
                {
                    productToRedact.QuantityPerUnit = productData.QuantityPerUnit;
                }
                if (productData.UnitPrice != null)
                {
                    productToRedact.UnitPrice = productData.UnitPrice;
                }
                if (productData.UnitsInStock != null)
                {
                    productToRedact.UnitsInStock = productData.UnitsInStock;
                }
                if (productData.UnitsOnOrder != null)
                {
                    productToRedact.UnitsOnOrder = productData.UnitsOnOrder;
                }
                if (productData.ReorderLevel != null)
                {
                    productToRedact.ReorderLevel = productData.ReorderLevel;
                }
                productToRedact.Discontinued = false;

                TempData["SuccessMessageRedact"] = "Entity was updated";
                _northwindContext.SaveChanges();

                return RedirectToAction("Product");
            }
        }

        public async Task<IActionResult> ProductDelete(int productId)
        {
            var productToDelete = await _northwindContext.Products.FirstOrDefaultAsync(p => p.ProductId == productId);

            if (productToDelete == null)
            {
                TempData["ErrorMessageDelete"] = "Product not found";
                return RedirectToAction("Product");
            }
            else
            {
                var orderDetailsToDelete = await _northwindContext.OrderDetails
                    .Where(o => o.ProductId == productId)
                    .ToListAsync();

                _northwindContext.OrderDetails.RemoveRange(orderDetailsToDelete);

                _northwindContext.Products.Remove(productToDelete);

                TempData["SuccessMessageDelete"] = "Product was deleted";
                _northwindContext.SaveChanges();

                return RedirectToAction("Product");
            }
        }

        public async Task<IActionResult> ProductAdd(Product newProduct)
        {
            newProduct.Discontinued = false;
            if (ModelState.IsValid)
            {
                _northwindContext.Products.Add(newProduct);
                await _northwindContext.SaveChangesAsync();
                TempData["SuccessMessageAdd"] = "Product added successfully";
            }
            else
            {
                TempData["ErrorMessageAdd"] = "Invalid product data";
            }

            return RedirectToAction("Product");
        }

        public async Task<IActionResult> SupplireRedact(Supplier supplierData)
        {
            var supplierToRedact = await _northwindContext.Suppliers.FirstOrDefaultAsync(s => s.SupplierId == supplierData.SupplierId);

            if (supplierToRedact == null)
            {
                TempData["ErrorMessageRedact"] = "Supplier not found";
                return RedirectToAction("Supplier");
            }
            else
            {
                if (supplierData.CompanyName != null)
                {
                    supplierToRedact.CompanyName = supplierData.CompanyName;
                }
                if (supplierData.ContactName != null)
                {
                    supplierToRedact.ContactName = supplierData.ContactName;
                }
                if (supplierData.ContactTitle != null)
                {
                    supplierToRedact.ContactTitle = supplierData.ContactTitle;
                }
                if (supplierData.Address != null)
                {
                    supplierToRedact.Address = supplierData.Address;
                }
                if (supplierData.City != null)
                {
                    supplierToRedact.City = supplierData.City;
                }
                if (supplierData.Region != null)
                {
                    supplierToRedact.Region = supplierData.Region;
                }
                if (supplierData.PostalCode != null)
                {
                    supplierToRedact.PostalCode = supplierData.PostalCode;
                }
                if (supplierData.Country != null)
                {
                    supplierToRedact.Country = supplierData.Country;
                }
                if (supplierData.Phone != null)
                {
                    supplierToRedact.Phone = supplierData.Phone;
                }
                if (supplierData.Fax != null)
                {
                    supplierToRedact.Fax = supplierData.Fax;
                }
                if (supplierData.HomePage != null)
                {
                    supplierToRedact.HomePage = supplierData.HomePage;
                }

                TempData["SuccessMessageRedact"] = "Entity was updated";
                _northwindContext.SaveChanges();

                return RedirectToAction("Supplier");
            }
        }

        public async Task<IActionResult> SupplierDelete(int SupplierId)
        {
            var supplierToDelete = await _northwindContext.Suppliers.FirstOrDefaultAsync(s => s.SupplierId  == SupplierId);

            if (supplierToDelete == null)
            {
                TempData["ErrorMessageDelete"] = "Supplier not found";
                return RedirectToAction("Supplier");
            }
            else
            {
                var productsToDelete = await _northwindContext.Products.Where(p => p.SupplierId == supplierToDelete.SupplierId).ToListAsync();

                _northwindContext.Suppliers.Remove(supplierToDelete);
                _northwindContext.Products.RemoveRange(productsToDelete);


                TempData["SuccessMessageDelete"] = "Product was deleted";
                _northwindContext.SaveChanges();

                return RedirectToAction("Supplier");
            }
        }


        public async Task<IActionResult> SupplierAdd(Supplier newSupplier)
        {
            if (ModelState.IsValid)
            {
                _northwindContext.Suppliers.Add(newSupplier);
                await _northwindContext.SaveChangesAsync();
                TempData["SuccessMessageAdd"] = "Supplier added successfully";
            }
            else
            {
                TempData["ErrorMessageAdd"] = "Invalid supplier data";
            }

            return RedirectToAction("Supplier");
        }
    }
}
