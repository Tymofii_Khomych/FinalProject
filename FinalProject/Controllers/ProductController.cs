﻿using FinalProject.AdditionalModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinalProject.Controllers
{
    public class ProductController : Controller
    {
        private readonly NorthwindContext _northwindContext;

        public ProductController(NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public async Task<IActionResult> Index()
        {
            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.ToListAsync();

            return View(results);
        }

        public IActionResult Reset()
        {
            HttpContext.Session.Remove("SelectedCountries");
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> OrderByName()
        {

            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.OrderBy(q => q.ProductName).ToListAsync();
            return View("Index", results);
        }

        public async Task<IActionResult> OrderByQuantity()
        {

            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.OrderBy(q => q.QuantityPerUnit).ToListAsync();
            return View("Index", results);
        }

        public async Task<IActionResult> OrderByPrice()
        {

            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.OrderBy(q => q.UnitPrice).ToListAsync();
            return View("Index", results);
        }

        public async Task<IActionResult> OrderByInStock()
        {

            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.OrderBy(q => q.UnitsInStock).ToListAsync();
            return View("Index", results);
        }

        public async Task<IActionResult> OrderByCategory()
        {

            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.OrderBy(q => q.CategoryName).ToListAsync();
            return View("Index", results);
        }

        public async Task<IActionResult> OrderByTotal()
        {

            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.OrderBy(q => q.TotalQuantity).ToListAsync();
            return View("Index", results);
        }

        [HttpPost]
        public async Task<IActionResult> FilterByPrice(int minPrice, int maxPrice)
        {
            var query = from od in _northwindContext.OrderDetails
                        join p in _northwindContext.Products on od.ProductId equals p.ProductId
                        join c in _northwindContext.Categories on p.CategoryId equals c.CategoryId
                        group od by new { p.ProductName, p.QuantityPerUnit, p.UnitPrice, p.UnitsInStock, c.CategoryName } into grouped
                        select new ProductViewModel
                        {
                            ProductName = grouped.Key.ProductName,
                            QuantityPerUnit = grouped.Key.QuantityPerUnit,
                            UnitPrice = grouped.Key.UnitPrice,
                            UnitsInStock = grouped.Key.UnitsInStock,
                            CategoryName = grouped.Key.CategoryName,
                            TotalQuantity = grouped.Sum(od => od.Quantity)
                        };

            var results = await query.Where(q => q.UnitPrice >= minPrice && q.UnitPrice <= maxPrice).ToListAsync();

            return View("Index", results);
        }

    }
}
