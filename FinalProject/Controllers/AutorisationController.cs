﻿using FinalProject.AdditionalModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace FinalProject.Controllers
{
    public class AutorisationController : Controller
    {
        private readonly NorthwindContext _northwindContext;

        public AutorisationController(NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Autorize(string login, string password)
        {
            var user = await _northwindContext.Users.FirstOrDefaultAsync(u => u.Login == login && 
                u.Password == HashingService.Hash(password));

            if (user != null)
            {
                return RedirectToAction("Index", "Mainpage");
            }
            else
            {
                ViewBag.ErrorMessage = "Користувача не знайдено";
                return View("~/Views/Autorisation/Index.cshtml");
            }
        }
    }
}