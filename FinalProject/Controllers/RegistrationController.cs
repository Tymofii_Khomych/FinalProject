﻿using FinalProject.AdditionalModels;
using FinalProject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinalProject.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly NorthwindContext _northwindContext;

        public RegistrationController(NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        
        [HttpPost]
        public async Task<IActionResult> Register(RegistrationBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var userWithLogin = await _northwindContext.Users.FirstOrDefaultAsync(u => u.Login == model.Login);
                if (userWithLogin != null)
                {
                    ModelState.AddModelError("Login", "User with this login already exists.");
                    return View("Index");
                }

                var userWithEmail = await _northwindContext.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                if (userWithEmail != null)
                {
                    ModelState.AddModelError("Email", "User with this email already exists.");
                    return View("Index");
                }

                var NewUser = new User(model.Login, model.Email, HashingService.Hash(model.Password));
                _northwindContext.Users.Add(NewUser);
                await _northwindContext.SaveChangesAsync();
                TempData["SuccessMessage"] = "Registration successful! You can now log in.";
                return RedirectToAction("Index", "Autorisation");
            }
            else
            {
                return View("Index");
            }
        }
    }
}
