﻿using FinalProject.AdditionalModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FinalProject.Controllers
{
    public class SuppliersController : Controller
    {
        private readonly NorthwindContext _northwindContext;

        public SuppliersController(NorthwindContext northwindContext)
        {
            _northwindContext = northwindContext;
        }

        public async Task<IActionResult> Index()
        {   
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByName()
        {
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.OrderBy(s => s.ContactName).ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByCompany()
        {
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.OrderBy(s => s.CompanyName).ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByCity()
        {
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.OrderBy(s => s.City).ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByCountry()
        {
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.OrderBy(s => s.Country).ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByProduct()
        {
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.OrderBy(p => p.ProductName).ToListAsync();
            return View("Index", model);
        }

        public async Task<IActionResult> OrderByQuantity()
        {
            var query = from supplier in _northwindContext.Suppliers
                        join product in _northwindContext.Products
                        on supplier.SupplierId equals product.SupplierId
                        select new SupplierViewModel
                        {
                            ContactName = supplier.ContactName,
                            CompanyName = supplier.CompanyName,
                            City = supplier.City,
                            Country = supplier.Country,
                            ProductName = product.ProductName,
                            QuantityPerUnit = product.QuantityPerUnit
                        };

            var model = await query.OrderBy(p => p.QuantityPerUnit).ToListAsync();
            return View("Index", model);
        }
    }
}
