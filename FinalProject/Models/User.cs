﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FinalProject.Models;

public partial class User
{
    public int? Id { get; set; }
    public string Login { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }

    public string? Role { get; set; }

    public User(string login, string email, string password)
    {
        Login = login;
        Email = email;
        Password = password;
        Role = "user";
    }
}
